package com.lrm.javan_modul3.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping
public class HelloWorldController {
    @GetMapping("/hello")
    public String helloWorld(){
        return "index_hello_world";
    }
}
